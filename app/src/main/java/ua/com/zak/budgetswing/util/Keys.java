package ua.com.zak.budgetswing.util;

/**
 * @author zak <zak@swingpulse.com>
 */
public class Keys {
    public static final String CATEGORY = "CATEGORY";
    public static final String ACCOUNT = "ACCOUNT";

    private Keys() { }
}
