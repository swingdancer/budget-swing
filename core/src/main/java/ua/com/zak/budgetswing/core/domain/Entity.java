package ua.com.zak.budgetswing.core.domain;

/**
 * @author zak <zak@swingpulse.com>
 */
public interface Entity {
    long getId();
    String getName();
}
