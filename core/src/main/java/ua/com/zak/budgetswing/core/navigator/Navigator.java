package ua.com.zak.budgetswing.core.navigator;

/**
 * @author zak <zak@swingpulse.com>
 */
public interface Navigator {
    void openAddTransactionScreen(NavigationBundle navigationBundle);

    void openAddCategoryScreen(NavigationBundle navigationBundle);

    void openAddAccountScreen(NavigationBundle navigationBundle);
}
